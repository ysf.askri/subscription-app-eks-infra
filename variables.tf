variable "region" {
  default     = "eu-west-3"
  description = "AWS region"
}

variable "cluster_name" {
  default     = "eksworkshop-eksctl-1"
  description = "EKS Cluster name"
}

variable "cluster_version" {
  default     = "1.22"
  description = "Kubernetes version"
}

variable "instance_type" {
  default     = "t3.small"
  description = "EKS node instance type"
}

variable "instance_count" {
  default     = 3
  description = "EKS node count"
}

variable "agent_version" {
  default     = "v14.10.0"
  description = "Agent version"
}

variable "agent_namespace" {
  default     = "my-gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  default     = "7gQmYn1tHBGArUam7HUm1rsyxmQL1LS3zJgLFsNLUNXkG_27yw"
  description = "Agent token (provided after registering an Agent in GitLab)"
  sensitive   = true
}

variable "kas_address" {
  default     = "wss://kas.gitlab.com"
  description = "Agent Server address (provided after registering an Agent in GitLab)"
}
